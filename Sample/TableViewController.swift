//
//  TableViewController.swift
//  Sample
//
//  Created by alfian0 on 23/11/18.
//  Copyright © 2018 alfian0. All rights reserved.
//

import UIKit

enum CellType: Int {
    case top
    case mid
    case bottom
}

class TableViewController: UITableViewController {
        
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "TopCell", bundle: nil), forCellReuseIdentifier: "TopCell")
        tableView.register(UINib(nibName: "MidCell", bundle: nil), forCellReuseIdentifier: "MidCell")
        tableView.register(UINib(nibName: "BottomCell", bundle: nil), forCellReuseIdentifier: "BottomCell")
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        guard let type = CellType(rawValue: section) else { return 0 }
        switch type {
        case .top, .mid, .bottom:
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let type = CellType(rawValue: indexPath.section) else { return UITableViewCell() }
        switch type {
        case .top:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TopCell", for: indexPath)
            return cell
        case .mid:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MidCell", for: indexPath)
            return cell
        case .bottom:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BottomCell", for: indexPath)
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let type = CellType(rawValue: indexPath.section) else { return 0 }
        switch type {
        case .top:
            return 200
        case .mid:
            return 100
        case .bottom:
            // MARK: By item count
            return 200
        }
    }
}
