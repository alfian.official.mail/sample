//
//  ViewController.swift
//  Sample
//
//  Created by alfian0 on 23/11/18.
//  Copyright © 2018 alfian0. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var ubahButton: UIButton!
    @IBOutlet weak var bottomView: UICollectionView!
    @IBOutlet weak var bottomHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ubahButton.addTarget(self, action: #selector(ubahHeight(_:)), for: .touchUpInside)
    }

    @objc func ubahHeight(_ sender: UIButton) {
        bottomHeightConstraint.constant = (bottomHeightConstraint.constant == 200) ? 500 : 200
        bottomView.backgroundColor = (bottomHeightConstraint.constant == 200) ? UIColor.brown : UIColor.yellow
    }
}
